# Sibyl: a Framework for Evaluating theImplementation of Routing Protocols in Fat-Trees

## What is the problem?

Routing solutions for data centers, which include protocol implementations and configurations, are difficult to evaluate
and test both for the density of fat-trees and for the complexity of the protocols. Also, since most issues show up only
when a fault happens, it is unfeasible to perform such tests in a production environment. Additionally, the lack of
standard testing procedures motivates an effort in developing solutions for such a critical task.

## What is Sibyl?

Sibyl is a software framework developed in order to perform a large number of experiments on several virtual fat-tree
configurations. Sibyl implements a methodology, specifically devised for testing routing protocols in fat-tree networks.

You can find more information about Sibyl and the methodologies in the paper: ["Sibyl: a Framework for Evaluating the
Implementation of Routing Protocols in Fat-Trees"](https://compunet.ing.uniroma3.it/assets/publications/Caiazzi-Scazzariello-Sibyl.pdf).

### Methodology features

The Methodology is based on the following milestones:

1) Fat-trees have regular topologies. This allows performing tests focused on specific elements of the topology to
   obtain results with a general value.
2) Per forming tests that measure actual times can be misleading since timings heavily depend on the used hardware.
   Also, assuming that the system clocks of nodes are perfectly synchronized is not realistic. Hence, we evaluate the
   implementations being oblivious as much as possible with respect to the actual timings.
3) Routing protocols can be conceptually very different. For this reason, we adopt a black-box method, disregarding, as
   much as possible, the internals of the implementations.

**N.B.** For detailed information on the Sibyl Methodology, please refer to the Sibyl Paper. 

### Tests

The methodology includes failure and recovery tests. Failure-tests induce many types of faults that are common in data
centers, and measure how implementations react to those faults. Recovery-tests measure how implementations react when
the network is restored. For each failure test, we:

1) Start the fabric, waiting convergence;
2) Cause the failure;
3) Capture all the Protocol Data Units  (PDUs)  until protocol convergence.

We perform the recovery tests analogously, focusing on the restoration of the regular operation.

More specifically the tests include:

- **Bootstrap**: The bootstrap of the fabric. Usuful to analyze the initialization of a protocol implementation.
- **Normal Operation**: A test in which no failures happen. Useful to analyze how the protocol implementation normally
  works.
- **Node Failure**: A single node failure. We consider the failure of a _Leaf_, of a Spine,and of a ToF.
- **Node Recovery**: Return to normal operation when a _Leaf_, a _Spine_, or a _ToF_ is restored after a failure.
- **Link Failure**: A single link failure. We consider failures of a _Leaf-Spine_ link and a _Spine-ToF_ link.
- **Link Recovery**: Return to normal operation when a _Leaf-Spine_ or a _Spine-ToF_ link is restored after a failure.
- **Partitioned Fabric**: Occurs when a _ToF_ node is completely severed from access prefixes of an entire PoD by
  multiple link failures. The aim is to verify the ability of an implementation to react to a Partitioned Fabric.

### Analysis

Sibyl also provide tools for analyzing the results of the tests. For each test:

1) We check the Convergence to verify that, when an event occurs, the fabric reaches the expected state.
2) We measure the Messaging Load injected into the network.
3) We focus on Locality to verify how “local” is such an overhead.
4) We focus on the Number of Rounds used by the implementation to converge.

**N.B.** To be agnostic to the internal operations of the implementations, Messaging Load, Locality, and Number of
Rounds analyses rely only on the signaling packets captured during the experiments. Such packets have different
features. First, they are either injected into the network as a consequence of an event  (e.g., BGP Update packets)  or
are exchanged independently on events that happen in the network (e.g., keep-alive packets). To analyze the reaction to
a specific event, the methodology only considers the packets that carry information related to it. Second, they may
travel on different levels of the network stack  (e.g., on TCP or directly on Ethernet). Lower-level packets that are
useless to track the reaction to a specific event (e.g., TCP SYNs or ACKs) are not considered.

More information on the Analysis and on the algorithms exploited to do such task can be found in the Sybil paper.

### Tools

Sibyl assembles existing tools with new ones.

1) [VFTGen](https://github.com/KatharaFramework/VFTGen): a tool to automatically generate and configure virtual fat-tree
   topologies. It takes as input the parameters of a fat-tree and generates as output a directory containing all the
   files needed to run the corresponding topology on Kathará. It is used in Step 1.
2) [Kathará](https://github.com/KatharaFramework/Kathara): a container-based network emulation system. It is the only
   available open-source emulator that supports Kubernetes as container orchestration system, and it can leverage on
   pure L2 networks. Hence, it allows emulating very large virtual networks in a distributed environment faithfully. It
   is used to accomplish Steps 2–9.
3) [Sibyl RT Calculator](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl-rt-calculator): a tool for
   generating the forwarding tables of a fat-tree. It takes as input a topology, a routing protocol, and a type of test,
   and returns the routing information for each node of the fat-tree. This information contains the server farm prefixes
   and the number of next hops that are expected to be computed according to the test. Sibyl RTCalculator is used for
   the convergence check in Steps 5 and 7.
4) [Sibyl Agent](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl-agent): a REST Service composed of
   a controller and several worker agents. An instance of worker is deployed one ach node of the fat-tree. The
   controller agent can perform actions on the nodes through the worker agents (e.g., causing an interface failure).
   Sibyl Agent is used in Steps 3–8.
5) [Sibyl Analyzer](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl-analyzer): a tool to analyze and
   plot the results of the experiments. Its input is the set of `.pcap` files containing the packets exchanged by the
   nodes during an experiment. It computes the metrics, saving them in a `.json` file. It also generates three
   interactive `.html` files containing the topology labeled with the number of packets on each link, the node-state
   timeline, and the node-state graph. Sibyl Analyzer can also be used to analyze data captured on physical nodes.

## Step of an experiment

During each experiment, Sibyl performs the following steps:

1) Generates a topology;
2) Deploys a set of containers and of virtual links representing the topology;
3) Begins, in each container, to capture all the generated PDUs on all the interfaces;
4) Starts the routing daemons on all the nodes with the suitable configuration;
5) Performs the convergence check;
6) Performs the required actions fora specific type of test;
7) Performs the convergence check again;
8) Ends the capture of PDUs and gathers the obtained.pcap files;
9) Shutdowns the containers;
10) Performs the analysis and outputs the results. Notice that the analyses of this step may be parallelized on all the
    results at a later stage.

### Available Protocol Implementations

Sibyl can run tests using the protocol implementations supported by _VFTGen_:

1) FRR BGP;
2) FRR Openfabric;
3) FRR IS-IS;
4) [RIFT-python](https://github.com/brunorijsman/rift-python) (the only available open-source implementation of the RIFT
   protocol).

## Prerequisites

- Python 3
- [Kathará](https://www.kathara.org/)

## Installation

Clone the repository and remember to init all the git submodules. To do that, after cloning the repo, enter the project
directory and type:

```shell
git pull --recurse-submodules
```

## Usage

Sibyl allows running repeated experiments on the specified scenarios. To run experiments on a single test, you have to
run `run_test.py` file. It accepts as input, in this order, the following mandatory parameters:

- `SCENARIO`: specify the type of test to perform. Possibles values are: 's1_bootstrap', 's2_normal_operation',
  's3a_leaf_node_failure', 's3b_spine_node_failure', 's3c_tof_node_failure', 's4a_leaf_node_recovery',
  's4b_spine_node_recovery', 's4c_tof_node_recovery', 's5a_leaf_spine_link_failure', 's5b_spine_tof_link_failure',
  's6a_leaf_spine_link_recovery', 's6b_spine_tof_link_recovery', 's7_partitioned_fabric'.
- `PROTOCOL`: specify the protocol used in the test. Possible values are: 'bgp', 'open_fabric', 'isis', 'rift'.
- `N_RUNS`: the number of experiments to be executed for the test.
- `K_LEAF`: the number of ports of a leaf node pointing north or south.
- `K_TOP`: the number of ports of a spine node pointing north or south.
- `R`: the number of ports of a spine node pointing north or south (redundancy factor).
- `SERVERS`: used to specify the number of servers for each leaf.

There is also an optional parameter:

- `[--kube_net]`: needed if you are using Kubernetes as virtualization engine in Kathará.

For example, to run an experiment of a Leaf Failure test on a small fat-tree, using BGP, enter the `src` folder and type
on terminal:

```shell
python3 run_test.py s3a_leaf_node_failure bgp 1 2 2 2 1

```

Sibyl also allows running repeated experiments on multiple tests. To run experiments on a multiple test, you have to
run `run_all_test.py` file. It accepts as input, in this order, the following mandatory parameters:

- `--topology`: A list of topologies separated by a space where execute the experiments. Use the following format '
  N_RUNS,K_LEAF,K_TOP,R [N_RUNS,K_LEAF,K_TOP,R ...]'
- `--protocol`: the protocol to use for the tests.

There are also optional parameters:

- `--exclude`: A list of tests to exclude. If no excludes are specified, Sibyl executes all the tests on the specified
  topologies.
- `[--kube_net]`: needed if you are using Kubernetes as virtualization engine in Kathará.

For example, to run 5 experiment on all the Node Failure tests, on several fat-trees, using BGP, enter the `src` folder
and type on terminal:

```shell
python3 run_all_tests.py --protocol bgp --exclude s1 s2 s4 s5 s6 s7 --topology 5,2,2,2 5,2,2,1 5,4,4,1

```

### Experiment Results

At the end of each experiment, you can find the logs and the dumps of the network traffic in a sub-folder of
the `analysis_results` directory named as the experiment you ran.

### Analyze the Results

To analyze the results of the tests, Sibyl provides the Sibyl Analyzer. In the Sibyl Analyzer folder (in the root
directory of the project), you can find the tools to analyze the results.

You can run sequential analysis running the `analyze_test_results.py` file. It accepts as mandatory input one of the two
following parameters:

- `PATH`: The path of an experiment result to analyze.
- `--base_path`: The path to a folder containing several tests results (e.g. the `analysis_results` folder).

By default, if the analyzer already find analysis result for an experiment it skip to analyze it. To avoid this
behaviour you can specify the `--no-cache` parameter.

You can also run **parallel analysis** using the `parallel_analyze_test_results.py` script. It takes the same parameters
of `analyze_test_results.py` but it performs the analysis in parallel.

The analysis produces a `.json` file for each experiment containing all the values of the computed metrics (Messaging
Load, Locality and Rounds, see the paper for further explanation on the metrics) and 3 `.html` files:

1) The 'radius-graph' containing the topology labeled with the number of packets on each link;
2) The 'node-state timeline' describing how the interactions of nodes evolves over time;
3) The 'node-state graph' describing the state changes of the nodes during the experiment.

### Plots of the Analysis

Sibyl also provides tools to plot the results of the analysis. To run the plotter enter the Sibyl Analyzer folder and
run `plot_test_results.py` specifying the analysis results folder:

```shell
 python3 plot_test_results.py ../analysis_results/
```

It generates plots with comparative analysis of the protocol implementations, giving a clear idea on how different
implementations react to a failure or a recovery. 
