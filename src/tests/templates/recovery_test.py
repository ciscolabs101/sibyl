import logging
from abc import ABC, abstractmethod

from rt_calculator import routing_table_calculator
from tests.decorators import after_convergence
from .failure_test import FailureTest


class RecoveryTest(ABC, FailureTest):
    @abstractmethod
    @after_convergence(FailureTest, priority=15)
    def make_recovery(self):
        raise NotImplementedError("You must implement `make_recovery` method.")

    @after_convergence(FailureTest, priority=20)
    def check_routing_tables_after_recovery(self):
        logging.info("Waiting protocol convergence after recovery...")

        expected_tables = routing_table_calculator.get_routing_tables(
            self.machine_interfaces.keys(), self.topology_info
        )

        if not self._check_protocol_convergence(expected_tables):
            raise Exception('Protocol not Converging!')
