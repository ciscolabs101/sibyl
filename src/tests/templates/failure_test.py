import logging
from abc import abstractmethod

from tests.decorators import after_convergence, before_start_routing
from tests.testcase import TestCase


class FailureTest(TestCase):
    @abstractmethod
    @before_start_routing(TestCase)
    def get_failure_info(self):
        raise NotImplementedError("You must implement `get_failure_info` method.")

    @abstractmethod
    @after_convergence(TestCase, priority=5)
    def make_failure(self):
        raise NotImplementedError("You must implement `make_failure` method.")

    @after_convergence(TestCase, priority=10)
    def check_routing_tables_after_failure(self):
        logging.info("Waiting protocol convergence after failure...")

        if 'failed_interfaces' not in self.results:
            running_machines = set(self.machine_interfaces.keys()) - {self.results['failed_machine']}
        else:
            running_machines = set(self.machine_interfaces.keys())

        expected_tables = self._expected_routing_tables(running_machines)

        if not self._check_protocol_convergence(expected_tables, machine_filters=running_machines):
            raise Exception('Protocol not Converging!')

    @abstractmethod
    def _expected_routing_tables(self, desired_machines):
        raise NotImplementedError("You must implement `_expected_routing_tables` method.")
