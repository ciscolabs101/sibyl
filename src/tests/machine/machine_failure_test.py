import logging
import os
import time
from abc import ABC

from callbacks.common import destroy_machine
from common.utils import extract_tar_stream
from tests.decorators import after_convergence
from tests.templates.failure_test import FailureTest


class MachineFailureTest(ABC, FailureTest):
    @after_convergence(FailureTest, priority=5)
    def make_failure(self):
        self.agent.stop_dumpers(machine_filter=[self.results['failed_machine']])

        machine_dumps = self.agent.get_dumps(machine_filter=[self.results['failed_machine']])
        shared_directory = os.path.join(self.lab_dir, 'shared')
        extract_tar_stream(machine_dumps, shared_directory)

        self.results['failure_time'] = self.agent.system_times()

        destroy_machine(self.results['failed_machine'], self.lab_dir)

        machine_running = self._check_running_machines(machine_name=self.results['failed_machine'])
        while machine_running > 0:
            logging.info('Waiting for machine `%s` shutdown...' % self.results['failed_machine'])
            time.sleep(5)
            machine_running = self._check_running_machines(machine_name=self.results['failed_machine'])
