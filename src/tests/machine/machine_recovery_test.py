import logging
import time
from abc import ABC

from callbacks.common import start_machine
from tests.decorators import after_convergence
from tests.templates.recovery_test import RecoveryTest
from .machine_failure_test import MachineFailureTest


class MachineRecoveryTest(RecoveryTest, MachineFailureTest, ABC):
    @after_convergence(RecoveryTest, priority=15)
    def make_recovery(self):
        running_machines = set(self.machine_interfaces.keys()) - {self.results['failed_machine']}
        self.results['recover_time'] = self.agent.system_times(machine_filter=running_machines)

        start_machine(self.results['failed_machine'], self.lab_dir)

        machine_pending = self._check_pending_machines(machine_name=self.results['failed_machine'])
        while machine_pending > 0:
            logging.info('Waiting for machine `%s` startup...' % self.results['failed_machine'])
            time.sleep(5)
            machine_pending = self._check_pending_machines(machine_name=self.results['failed_machine'])

        self.agent.update_nodes()
        time.sleep(10)

        self.results['recover_time'].update(self.agent.system_times(machine_filter=[self.results['failed_machine']]))

        self._get_machines_interfaces_mac_addresses(machine_filter=[self.results['failed_machine']])

        logging.info('Restarting dumper on `%s`...' % self.results['failed_machine'])
        self.agent.start_dumpers(
            {self.results['failed_machine']: self.machine_interfaces[self.results['failed_machine']]},
            self.topology_info['protocol'],
            machine_filter=[self.results['failed_machine']]
        )

        logging.info('Restarting routing daemon on `%s`...' % self.results['failed_machine'])
        self.agent.start_protocol(self.topology_info['protocol'], machine_filter=[self.results['failed_machine']])
