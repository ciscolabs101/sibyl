from abc import ABC

from callbacks.common import enable_link
from tests.decorators import after_convergence
from tests.templates.recovery_test import RecoveryTest
from .link_failure_test import LinkFailureTest


class LinkRecoveryTest(RecoveryTest, LinkFailureTest, ABC):
    @after_convergence(RecoveryTest, priority=15)
    def make_recovery(self):
        self.results['recover_time'] = self.agent.system_times()
        
        for interface in self.results['failed_interfaces']:
            enable_link(self.results['failed_machine'], interface, self.lab_dir)
