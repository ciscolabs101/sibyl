from abc import ABC

from callbacks.common import destroy_link
from tests.decorators import after_convergence
from tests.templates.failure_test import FailureTest


class LinkFailureTest(ABC, FailureTest):
    @after_convergence(FailureTest, priority=5)
    def make_failure(self):
        self.results['failure_time'] = self.agent.system_times()

        for interface in self.results['failed_interfaces']:
            destroy_link(self.results['failed_machine'], interface, self.lab_dir)
