import glob
import logging
import os
import shlex
import sys
import time
from pathlib import Path
from subprocess import DEVNULL, PIPE, Popen

import coloredlogs
from sortedcontainers import SortedDict

from agent.agent import Agent
from common.routing_table_utils import compare_routing_tables
from common.utils import (format_headers, write_test_results, copy_results, KATHARA_PATH, CONVERGENCE_ATTEMPTS,
                          copy_lab_json, extract_tar_stream, generate_timestamped_name)
from rt_calculator import routing_table_calculator
from vftgen.src.utils import create_fat_tree


class TestCase(object):
    __slots__ = [
        'test_id', 'results', 'lab_dir', 'topology_info', 'machine_interfaces', 'agent'
    ]

    callbacks = {
        'before_start_routing': SortedDict(),
        'before_convergence': SortedDict(),
        'after_convergence': SortedDict(),
        'on_complete': SortedDict()
    }

    def __init__(self):
        self.test_id = 1

        self.results = {}

        self.lab_dir = None

        self.topology_info = None
        self.machine_interfaces = {}

        self.agent = None

        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

        # noinspection PyArgumentList
        logging.basicConfig(
            format='[%(asctime)s] %(levelname)s - %(message)s',
            level=logging.INFO,
            handlers=[
                logging.StreamHandler(sys.stdout)
            ]
        )
        coloredlogs.install(fmt='[%(asctime)s] %(levelname)s - %(message)s', level="INFO")

    def run(self, k_leaf, k_top, redundancy_factor, servers_for_rack, protocol, is_k8s):
        self.results['k_leaf'] = k_leaf
        self.results['k_top'] = k_top
        self.results['redundancy_factor'] = redundancy_factor
        self.results['is_k8s'] = is_k8s

        protocol_display_name = "".join(map(lambda x: x.capitalize(), protocol.split("_")))
        suffix = '-%s_%s_%s' % (k_leaf, k_top, redundancy_factor)
        test_name = protocol_display_name + self.__class__.__name__ + suffix

        current_file_path = os.path.dirname(Path(__file__).absolute())
        current_path = os.path.abspath(os.path.join(current_file_path, os.path.join("..", "..")))

        results_dir = os.path.join(current_path, 'analysis_results')
        test_results_directory = os.path.join(results_dir, '%s_results' % test_name)
        os.makedirs(test_results_directory, exist_ok=True)

        run_timestamped_name = generate_timestamped_name('run%d' % self.test_id)
        logging_handler = logging.FileHandler(os.path.join(test_results_directory, "%s.log" % run_timestamped_name))
        logging_handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s - %(message)s'))
        logging.root.addHandler(logging_handler)

        output_directory = os.path.join(current_path, 'laboratories')

        start_time = time.time()
        logging.info(format_headers("Running test %s" % test_name))

        agent_path = os.path.join(current_path, "sibyl_agent")
        self.agent = Agent(agent_path)
        logging.info("Checking if test agents are running...")
        self.agent.check_and_start_agents(is_k8s)

        logging.info("Generating Fat-Tree Topology with K_LEAF=%d, K_TOP=%d, R=%d and %s protocol..." %
                     (k_leaf, k_top, redundancy_factor, protocol_display_name)
                     )

        self.topology_info, test_dir, self.lab_dir = create_fat_tree({
            "k_leaf": k_leaf,
            "k_top": k_top,
            "redundancy_factor": redundancy_factor,
            "servers_for_rack": servers_for_rack,
            "tof_rings": True if "rift" in protocol and (k_leaf / redundancy_factor) != 1 else False,
            "protocol": protocol
        }, output_dir_name=output_directory, is_k8s=is_k8s)

        self._edit_lab()

        try:
            logging.info("Deploying Fat-Tree in dir: %s" % self.lab_dir)
            os.system("%s lstart --noterminals -d \"%s\"" % (KATHARA_PATH, self.lab_dir))
            logging.info("Fat-Tree deployed!")

            pending_machines = self._check_pending_machines()
            while pending_machines > 0:
                logging.info("%d machines not started." % pending_machines)
                time.sleep(5)
                pending_machines = self._check_pending_machines()

            logging.info("Starting test agent on machines...")
            self.agent.start(len([x for x in self._get_lab_machines() if 'server' not in x]))

            logging.info("Getting machines information...")
            self._get_machines_interfaces()
            self._get_machines_interfaces_mac_addresses()

            logging.info("Starting packet dumpers...")
            self.agent.start_dumpers(self.machine_interfaces, protocol)

            for _, before_routing_callbacks in self.callbacks['before_start_routing'].items():
                for before_routing_callback in before_routing_callbacks:
                    before_routing_callback(self)

            logging.info("Starting routing daemon on machines...")
            self.agent.start_protocol(protocol)

            for _, before_convergence_callbacks in self.callbacks['before_convergence'].items():
                for before_convergence_callback in before_convergence_callbacks:
                    before_convergence_callback(self)

            logging.info("Waiting protocol convergence...")
            expected_tables = routing_table_calculator.get_routing_tables(self.machine_interfaces.keys(),
                                                                          self.topology_info)

            if not self._check_protocol_convergence(expected_tables):
                raise Exception('Protocol not Converging!')

            for _, after_convergence_callbacks in self.callbacks['after_convergence'].items():
                for after_convergence_callback in after_convergence_callbacks:
                    after_convergence_callback(self)

            logging.info("Stopping packet dumpers...")
            self.agent.stop_dumpers()

            logging.info("Collecting dumps from nodes...")
            dumps = self.agent.get_dumps()

            shared_directory = os.path.join(self.lab_dir, 'shared')
            extract_tar_stream(dumps, shared_directory)

            self._undeploy_scenario()

            for _, on_complete_callbacks in self.callbacks['on_complete'].items():
                for on_complete_callback in on_complete_callbacks:
                    on_complete_callback(self)

            write_test_results(self.results, test_name, run_timestamped_name)

            copy_lab_json(test_dir, test_results_directory)
            copy_results(test_results_directory, run_timestamped_name, shared_directory)
        except Exception as e:
            logging.error(str(e))
            self._undeploy_scenario()
        finally:
            logging.info(format_headers("End Test %s" % test_name))
            logging.info(format_headers("Execution Time: %s seconds" % (time.time() - start_time)))
            logging.root.removeHandler(logging_handler)

    def run_n_times(self, n_runs, k_leaf, k_top, redundancy_factor, servers_for_rack, protocol, is_k8s):
        for i in range(1, n_runs + 1):
            logging.info(format_headers("Run #%d" % i))
            self.test_id = i
            self.run(k_leaf, k_top, redundancy_factor, servers_for_rack, protocol, is_k8s)
            logging.info(format_headers("End Run #%d" % i))

    @staticmethod
    def _check_pending_machines(machine_name=None):
        machine_grep = ""
        if machine_name:
            machine_grep = "grep \'%s \' |" % machine_name

        pending_machines_process = Popen(shlex.split(
            "bash -c \"export PYTHONIOENCODING=utf8; "
            "%s list | egrep -vi '[\u2566|\u2550|\u256c]' | tail -n +4 | "
            "grep -vi running | grep -v controller_agent | %s wc -l\"" %
            (KATHARA_PATH, machine_grep)
        ), stdout=PIPE, stderr=DEVNULL)
        pending_machines = int(pending_machines_process.stdout.readline().decode('utf-8').strip())
        pending_machines_process.terminate()

        return pending_machines

    @staticmethod
    def _check_running_machines(machine_name=None):
        machine_grep = ""
        if machine_name:
            machine_grep = "grep \'%s \' |" % machine_name

        command = "bash -c \"export PYTHONIOENCODING=utf8; " \
                  "%s list | grep -i running | grep -v controller_agent | %s wc -l\"" % (KATHARA_PATH, machine_grep)

        running_machines_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        running_machines = int(running_machines_process.stdout.readline().decode('utf-8').strip())
        running_machines_process.terminate()

        return running_machines

    def _edit_lab(self):
        # Add bridged interface to all the devices (not servers) when using Docker
        if not self.results['is_k8s']:
            self._edit_lab_conf()

        # Remove protocol init and add worker test agent initialization
        self._edit_machine_startups()

        # Create a proper resolv.conf file pointing at k8s DNS when on Megalos
        if self.results['is_k8s']:
            for machine_name in self._get_lab_machines(only_name=True):
                self.agent.create_dns_file(self.lab_dir, machine_name)

    def _edit_lab_conf(self):
        with open(os.path.join(self.lab_dir, 'lab.conf'), 'a') as lab_conf_file:
            for machine in self._get_lab_machines(only_name=True):
                if 'server' not in machine:
                    lab_conf_file.write('%s[bridged]="true"\n' % machine)

    def _edit_machine_startups(self):
        machines_startup = self._get_lab_machines()
        for machine_startup in machines_startup:
            with open(machine_startup, 'r') as file:
                lines = file.readlines()

            lines = list(
                filter(
                    lambda x: 'frr start' not in x and 'rift/rift' not in x and '/etc/juniper_rift.conf' not in x,
                    lines
                )
            )

            with open(machine_startup, 'w') as file:
                file.write("ip route del default\n")
                file.writelines(lines)

                if 'server' not in machine_startup:
                    self.agent.append_worker_startup_lines(file, is_k8s=self.results['is_k8s'])

    def _get_machines_interfaces(self):
        machine_interfaces = self.agent.get_network_interfaces()
        interface_prefix = 'net' if self.results['is_k8s'] else 'eth'

        for machine_name, interfaces in machine_interfaces.items():
            self.machine_interfaces[machine_name] = sorted(map(lambda x: x['ifname'],
                                                               filter(lambda x: interface_prefix in x['ifname'],
                                                                      interfaces)),
                                                           key=lambda x: int(x.replace(interface_prefix, '')))

            # Remove bridged interface if using Docker (the last one)
            if not self.results['is_k8s']:
                self.machine_interfaces[machine_name] = self.machine_interfaces[machine_name][:-1]

    def _get_machines_interfaces_mac_addresses(self, machine_filter=None):
        machine_interfaces_mac_addresses = self.agent.get_interfaces_mac_addresses(machine_filter=machine_filter)
        interface_prefix = 'net' if self.results['is_k8s'] else 'eth'

        if 'machine_mac_addresses' not in self.results:
            self.results['machine_mac_addresses'] = {}

        for machine_name, interfaces_mac_addresses in machine_interfaces_mac_addresses.items():
            self.results['machine_mac_addresses'][machine_name] = SortedDict()

            for interface_name, mac_address in interfaces_mac_addresses.items():
                if interface_prefix in interface_name:
                    self.results['machine_mac_addresses'][machine_name][interface_name] = mac_address

            if not self.results['is_k8s']:
                last_interface = self.results['machine_mac_addresses'][machine_name].keys()[-1]
                self.results['machine_mac_addresses'][machine_name].pop(last_interface)

    def _get_lab_machines(self, only_name=False):
        startups = glob.glob(os.path.join(self.lab_dir, '*.startup'))

        return startups if not only_name \
            else list(map(lambda startup: os.path.basename(startup).replace('.startup', ''), startups))

    def _check_protocol_convergence(self, expected_tables, machine_filters=None):
        attempts = 0
        counter = 0
        while not counter >= 3:
            real_tables = self.agent.get_routing_table(machine_filter=machine_filters)
            time.sleep(5)

            if attempts >= CONVERGENCE_ATTEMPTS:
                logging.warning("Protocol is not converging: %d attempts. Aborting Experiment." % CONVERGENCE_ATTEMPTS)
                return False
            if compare_routing_tables(real_tables, expected_tables):
                counter += 1
            else:
                counter = 0
                attempts += 1

        time.sleep(5)
        logging.info("Protocol converged!")

        return True

    def _undeploy_scenario(self):
        logging.info("Undeploying Fat-Tree...")
        os.system("%s lclean -d %s" % (KATHARA_PATH, self.lab_dir))

        running_machines = self._check_running_machines()
        while running_machines > 0:
            logging.info("%d machines still running." % running_machines)
            time.sleep(5)
            running_machines = self._check_running_machines()

        logging.info("Fat-Tree undeployed!")
