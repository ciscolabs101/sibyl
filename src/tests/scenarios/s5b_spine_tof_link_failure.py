import logging

from rt_calculator import routing_table_calculator
from tests.decorators import before_start_routing
from tests.link.link_failure_test import LinkFailureTest


class SpineTofLinkFailure(LinkFailureTest):
    @before_start_routing(LinkFailureTest)
    def get_failure_info(self):
        self.results['failed_machine'] = 'spine_1_1_1'
        # The k_top-th interface is the link attached to the first ToF
        failed_interface = self.machine_interfaces[self.results['failed_machine']][self.topology_info['k_top']]
        self.results['failed_interfaces'] = [failed_interface]

        logging.info('The chosen interface to destroy is %s of %s.' %
                     (failed_interface, self.results['failed_machine'])
                     )

    def _expected_routing_tables(self, desired_machines):
        failed_interface = self.results['failed_interfaces'][0]

        return routing_table_calculator.get_routing_tables_in_spine_tof_link_failure(
            desired_machines, self.topology_info, self.results['failed_machine'], failed_interface
        )
