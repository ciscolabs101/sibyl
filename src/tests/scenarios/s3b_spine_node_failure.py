import logging

from rt_calculator import routing_table_calculator
from tests.decorators import before_start_routing
from tests.machine.machine_failure_test import MachineFailureTest


class SpineNodeFailure(MachineFailureTest):
    @before_start_routing(MachineFailureTest)
    def get_failure_info(self):
        self.results['failed_machine'] = 'spine_1_1_1'

        logging.info('The chosen machine to destroy is %s.' % self.results['failed_machine'])

    def _expected_routing_tables(self, desired_machines):
        return routing_table_calculator.get_routing_tables_in_spine_failure(
            desired_machines, self.topology_info, self.results['failed_machine']
        )
