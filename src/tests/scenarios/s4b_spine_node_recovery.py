from tests.machine.machine_recovery_test import MachineRecoveryTest
from tests.scenarios.s3b_spine_node_failure import SpineNodeFailure


class SpineNodeRecovery(SpineNodeFailure, MachineRecoveryTest):
    pass
