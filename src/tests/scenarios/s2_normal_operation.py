from time import sleep
import logging

from .s1_bootstrap import Bootstrap
from ..decorators import after_convergence


class NormalOperation(Bootstrap):
    @after_convergence(Bootstrap)
    def wait_for_protocol_packets(self):
        logging.info("Waiting 300s for capturing normal operation PDUs...")
        sleep(300)
