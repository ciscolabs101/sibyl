from tests.machine.machine_recovery_test import MachineRecoveryTest
from tests.scenarios.s3c_tof_node_failure import TofNodeFailure


class TofNodeRecovery(TofNodeFailure, MachineRecoveryTest):
    pass
