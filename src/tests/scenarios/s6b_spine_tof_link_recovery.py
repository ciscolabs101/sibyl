from tests.link.link_recovery_test import LinkRecoveryTest
from tests.scenarios.s5b_spine_tof_link_failure import SpineTofLinkFailure


class SpineTofLinkRecovery(SpineTofLinkFailure, LinkRecoveryTest):
    pass
