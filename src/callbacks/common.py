import logging
import os

# noinspection PyUnresolvedReferences
from parent_import import parentdir

from common.client import Client
from common.utils import KATHARA_PATH


def destroy_machine(machine_name, lab_dir):
    logging.info('Destroying machine %s...' % machine_name)
    destroy_command = '%s lclean -d "%s" %s' % (KATHARA_PATH, lab_dir, machine_name)
    os.system(destroy_command)


def start_machine(machine_name, lab_dir):
    logging.info('Starting machine %s...' % machine_name)
    start_command = '%s lstart --noterminals -d "%s" %s' % (KATHARA_PATH, lab_dir, machine_name)
    os.system(start_command)


def destroy_link(machine_name, interface, lab_dir):
    logging.info('Destroying link %s of machine %s...' % (interface, machine_name))
    destroy_command = "ifconfig %s down" % interface
    Client.exec_async(lab_dir, machine_name, destroy_command)


def enable_link(machine_name, interface, lab_dir):
    logging.info('Enabling link %s of machine %s...' % (interface, machine_name))
    start_command = 'ifconfig %s up' % interface
    Client.exec_async(lab_dir, machine_name, start_command)
