import os
import shlex
from subprocess import Popen, DEVNULL

from common.utils import KATHARA_PATH


class Client(object):
    @staticmethod
    def exec_sync(lab_dir, machine_name, command, suppress_output=False):
        exec_command = '%s exec -d "%s" %s -- /bin/bash -c \'%s\'' % \
                       (KATHARA_PATH, lab_dir, machine_name, command)

        if suppress_output:
            exec_command += ' > /dev/null 2>&1'

        os.system(exec_command)

    @staticmethod
    def exec_async(lab_dir, machine_name, command, stdout=DEVNULL, stderr=DEVNULL):
        return Popen(
            shlex.split('%s exec -d "%s" %s -- /bin/bash -c \'%s\'' %
                        (KATHARA_PATH, lab_dir, machine_name, command)
                        ),
            stdout=stdout,
            stderr=stderr
        )
