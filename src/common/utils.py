import importlib
import io
import json
import logging
import os
import shutil
import tarfile
import time
from itertools import islice
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from pathlib import Path

environment_path = os.environ.get('KATHARA_PATH')
KATHARA_PATH = environment_path if environment_path else '/usr/bin/kathara'

environment_convergence_attempts = os.environ.get('CONVERGENCE_ATTEMPTS')
CONVERGENCE_ATTEMPTS = int(environment_convergence_attempts) if environment_convergence_attempts else 100

current_file_path = os.path.dirname(Path(__file__).absolute())
current_path = os.path.abspath(os.path.join(current_file_path, os.path.join("..", "..")))


def generate_timestamped_name(prefix, ext=''):
    (year, month, day, hour, minutes, _, _, _, _) = time.localtime(time.time())
    return '%s-%d-%s-%s-%s_%s%s' % (prefix, year, str(month).zfill(2), str(day).zfill(2), str(hour).zfill(2),
                                    str(minutes).zfill(2), ext)


def write_test_results(data, test_name, timestamped_file_name):
    results_file_name = "%s.json" % timestamped_file_name

    _write_file_in_results_dir(data, test_name, results_file_name)


def copy_results(test_results_directory, timestamped_file_name, shared_directory):
    os.makedirs(test_results_directory, exist_ok=True)

    dumps_path = os.path.join(test_results_directory, 'dumps-%s' % timestamped_file_name)
    shutil.move(shared_directory, dumps_path)


def copy_lab_json(test_dir, results_directory):
    lab_json = os.path.join(test_dir, "lab.json")
    shutil.copy(lab_json, results_directory)


def extract_tar_stream(tar_stream, dest_dir):
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)

    dumps_stream = io.BytesIO(tar_stream)
    with tarfile.open(fileobj=dumps_stream) as dumps_tar:
        for member in dumps_tar.getmembers():
            if member.name.startswith('/'):
                member.name = member.name.strip('/')

            dumps_tar.extract(member, path=dest_dir)

    dumps_stream.close()

    def callback(file):
        if '.tar.gz' not in file:
            return

        name = file.replace('.tar.gz', '')
        file_path = os.path.join(dest_dir, file)

        with tarfile.open(file_path) as dump_tar:
            for dump_member in dump_tar.getmembers():
                if dump_member.name.startswith('/'):
                    dump_member.name = dump_member.name.strip('/')

                dump_tar.extract(dump_member, path=os.path.join(dest_dir, name))

        os.remove(file_path)

    pool = Pool(cpu_count())
    pool.map(func=callback, iterable=os.listdir(dest_dir))


def _write_file_in_results_dir(data, test_name, file_name):
    global current_path
    results_dir = os.path.join(current_path, 'analysis_results')
    test_results_directory = os.path.join(results_dir, '%s_results' % test_name)
    os.makedirs(test_results_directory, exist_ok=True)

    results_file_path = os.path.join(test_results_directory, "%s" % file_name)

    with open(results_file_path, 'w') as results:
        results.write(json.dumps(data, indent=4))

    logging.info("Results File written in %s" % results_file_path)


def format_headers(message=""):
    footer = "=============================="
    half_message = int((len(message) / 2) + 1)
    second_half_message = half_message

    if len(message) % 2 == 0:
        second_half_message -= 1

    message = " " + message + " " if message != "" else "=="
    return footer[half_message:] + message + footer[second_half_message:]


def class_for_name(module_name, class_name):
    m = importlib.import_module(module_name)
    return getattr(m, class_name)


def _list_chunks(iterable, size):
    it = iter(iterable)
    item = list(islice(it, size))
    while item:
        yield item
        item = list(islice(it, size))


def chunk_list(iterable, size):
    return [iterable] if len(iterable) < size else _list_chunks(iterable, size)
