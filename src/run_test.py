#!/usr/bin/python3

import argparse
import logging
import sys

from common import utils

AVAILABLE_SCENARIOS = [
    's1_bootstrap',
    's2_normal_operation',
    's3a_leaf_node_failure',
    's3b_spine_node_failure',
    's3c_tof_node_failure',
    's4a_leaf_node_recovery',
    's4b_spine_node_recovery',
    's4c_tof_node_recovery',
    's5a_leaf_spine_link_failure',
    's5b_spine_tof_link_failure',
    's6a_leaf_spine_link_recovery',
    's6b_spine_tof_link_recovery',
    's7_partitioned_fabric',
]


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description="Run tests on a specified scenario with a protocol.")
    parser.add_argument('scenario', metavar='SCENARIO', type=str, choices=AVAILABLE_SCENARIOS,
                        help=" ".join(AVAILABLE_SCENARIOS))
    parser.add_argument('protocol', metavar='PROTOCOL', type=str,
                        choices=['bgp', 'isis', 'open_fabric', 'rift', 'juniper_rift'])
    parser.add_argument('n_runs', metavar='N_RUNS', type=int, nargs=1)
    parser.add_argument('k_leaf', metavar='K_LEAF', type=int, nargs=1)
    parser.add_argument('k_top', metavar='K_TOP', type=int, nargs=1)
    parser.add_argument('redundancy_factor', metavar='R', type=int, nargs=1)
    parser.add_argument('servers_for_rack', metavar='SERVERS', type=int, nargs=1)
    parser.add_argument('--kube_net', action='store_true', default=False, required=False)

    parsed_args = parser.parse_args(argv)
    parsed_args.n_runs = parsed_args.n_runs.pop()
    parsed_args.k_leaf = parsed_args.k_leaf.pop()
    parsed_args.k_top = parsed_args.k_top.pop()
    parsed_args.redundancy_factor = parsed_args.redundancy_factor.pop()
    parsed_args.servers_for_rack = parsed_args.servers_for_rack.pop()

    return parsed_args


def run_scenario(scenario, protocol, n_runs, k_leaf, k_top, redundancy_factor, servers_for_rack, is_k8s):
    if 's7' in scenario and (k_leaf / redundancy_factor) == 1:
        logging.warning("Can't run Partitioned Fabric test on Single Plane Fat-Trees!")
        return

    module_name = "tests.scenarios.%s" % scenario
    class_name = "".join(map(lambda x: x.capitalize(), scenario.split("_")[1:]))
    test_class = utils.class_for_name(module_name, class_name)

    test = test_class()
    test.run_n_times(n_runs, k_leaf, k_top, redundancy_factor, servers_for_rack, protocol, is_k8s)


if __name__ == '__main__':
    args = parse_arguments(sys.argv[1:])

    run_scenario(args.scenario, args.protocol, args.n_runs, args.k_leaf, args.k_top, args.redundancy_factor,
                 args.servers_for_rack, args.kube_net)
