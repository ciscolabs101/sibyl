import json
import logging
import os
import time

import requests

from common.utils import chunk_list
from .bin_agent import BinAgent
from .controller_agent import ControllerAgent


class Agent(object):
    __slots__ = ['_bin_agent', '_controller_agent', '_nodes', '_pids']

    def __init__(self, agent_path):
        self._bin_agent = BinAgent(os.path.join(agent_path, "test-agent-lab"))
        self._controller_agent = ControllerAgent(os.path.join(agent_path, "test-agent-lab"))

        self._nodes = {}
        self._pids = {}

    def check_and_start_agents(self, is_k8s):
        self._bin_agent.start(is_k8s)
        self._controller_agent.check_and_start(is_k8s)

        self._flush()

    def create_dns_file(self, lab_path, machine_name):
        etc_path = os.path.join(lab_path, machine_name, 'etc')

        os.makedirs(etc_path, exist_ok=True)
        with open(os.path.join(etc_path, 'resolv.conf'), 'w') as resolv_file:
            resolv_file.write('nameserver 10.96.0.10\n')
            resolv_file.write('search default.svc.cluster.local %s.svc.cluster.local' % self._controller_agent.lab_hash)

    def append_worker_startup_lines(self, startup_file, is_k8s):
        if is_k8s:
            # Add a route to the default k8s service subnet
            startup_file.write(
                '\n' +
                'route add -net 10.96.0.0/12 gw '
                '$(ip -4 addr show dev eth0 | grep -oP \'(?<=inet\\s)\\d+(\\.\\d+){3}\' | '
                'awk -F "." \'{ print $1"."$2"."$3".1" }\') '
                'dev eth0'
            )

        if is_k8s:
            startup_file.write(
                '\n' +
                'tar xfvz /shared/test_agent.tar.gz -C /; '
            )
        else:
            startup_file.write(
                '\n' +
                ('curl -s -o test_agent.tar.gz %s; ' % self._bin_agent.get_internal_url(is_k8s)) +
                'tar xfvz test_agent.tar.gz -C /; ' +
                'rm test_agent.tar.gz; '
            )

        startup_file.write(
            '\n' +
            '%s /test_agent/agent &' % ('CONTROLLER_URL="%s"' % self._controller_agent.get_internal_url(is_k8s))
        )

    def start(self, nodes_num):
        nodes = self._get_joined_nodes()
        while len(nodes.keys()) != nodes_num:
            time.sleep(2)
            nodes = self._get_joined_nodes()

            logging.info("%d/%d nodes joined." % (len(nodes.keys()), nodes_num))

        self._flush()

        self._nodes.update(self._fix_machine_names(nodes))

    def update_nodes(self):
        self._nodes.update(self._fix_machine_names(self._get_joined_nodes()))

    def system_times(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        result = None
        while not result:
            try:
                response = requests.post(self._build_url('system-time'), json={'nodes': nodes}, timeout=None)
                result = response.json()
            except json.decoder.JSONDecodeError:
                pass

        return {key: value['time'] for key, value in result.items()}

    def get_network_interfaces(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        result = None
        while not result:
            try:
                response = requests.post(self._build_url('network-interfaces'), json={'nodes': nodes}, timeout=None)
                result = response.json()
            except json.decoder.JSONDecodeError:
                pass

        return result

    def get_interfaces_mac_addresses(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        result = None
        while not result:
            try:
                response = requests.post(self._build_url('interfaces-mac-addresses'), json={'nodes': nodes},
                                         timeout=None)
                result = response.json()
            except json.decoder.JSONDecodeError:
                pass

        return result

    def get_routing_table(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        chunks = chunk_list(nodes.items(), 200)
        routing_tables = {}

        for chunk in chunks:
            result = None
            while not result:
                try:
                    response = requests.post(self._build_url('routing-table'), json={'nodes': dict(chunk)},
                                             timeout=None)
                    result = response.json()
                except json.decoder.JSONDecodeError:
                    pass

            routing_tables.update(result)

        return routing_tables

    def start_protocol(self, protocol, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        requests.post(self._build_url('start-protocol'), json={'nodes': nodes, 'protocol': protocol}, timeout=None)

    def start_dumpers(self, machine_interfaces, protocol, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)

        result = None
        while not result:
            try:
                response = requests.post(self._build_url('start-dumpers'),
                                         json={'nodes': nodes, 'interfaces': machine_interfaces, 'protocol': protocol},
                                         timeout=None)
                result = response.json()
            except json.decoder.JSONDecodeError:
                pass

        self._pids.update(result)

    def stop_dumpers(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)
        pids = self._filter_dict(self._pids, machine_filter)

        requests.post(self._build_url('stop-dumpers'), json={'nodes': nodes, 'dumper_pids': pids}, timeout=None)

    def get_dumps(self, machine_filter=None):
        nodes = self._filter_dict(self._nodes, machine_filter)
        dumps_file_path = os.path.join('/tmp', 'dumps.tar.gz')

        with requests.post(self._build_url('get-dumps'), json={'nodes': nodes}, timeout=None, stream=True) as r:
            r.raise_for_status()

            with open(dumps_file_path, 'wb') as dumps_file:
                for chunk in r.iter_content(chunk_size=1024 * 1024):
                    dumps_file.write(chunk)

        with open(dumps_file_path, 'rb') as dumps_file:
            dumps = dumps_file.read()

        os.remove(dumps_file_path)

        return dumps

    def _get_joined_nodes(self):
        try:
            response = requests.get(self._build_url('nodes'), timeout=None)
            return response.json()
        except Exception:
            return {}

    def _flush(self):
        request_okay = False

        while not request_okay:
            try:
                requests.get(self._build_url('flush'), timeout=5)
                request_okay = True
            except Exception:
                pass

    def _build_url(self, request):
        if self._controller_agent.host_url is None:
            raise Exception('Agent URL is not set.')

        return '%s/%s' % (self._controller_agent.host_url, request)

    @staticmethod
    def _filter_dict(value, filter_set):
        filter_set = None if filter_set is None else set(filter_set)
        return {key: value for key, value in value.items() if key in filter_set} if filter_set else value

    @staticmethod
    def _fix_machine_names(response):
        new_response = {}

        for key, value in response.items():
            new_key = key.replace('kathara-', '')
            if '-' in new_key:
                parts = new_key.split('-')
                new_key = "%s_%s_%s_%s" % (parts[0], parts[1], parts[2], parts[3])

            new_response[new_key] = value

        return new_response
