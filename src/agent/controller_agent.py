import json
import logging
import os
import shlex
import time
from subprocess import Popen, PIPE, DEVNULL

from common.client import Client
from common.utils import KATHARA_PATH


class ControllerAgent(object):
    __slots__ = ['lab_path', 'host_url', 'lab_hash', '_internal_url']

    def __init__(self, lab_path):
        self.lab_path = lab_path
        self.host_url = None
        self.lab_hash = None

        self._internal_url = None

    def check_and_start(self, is_k8s):
        agent_output = self._check()

        if not agent_output:
            logging.info("Controller test agent not running, starting...")
            self._start()
            logging.info("Controller test agent started successfully...")

        agent_output = self._check()
        while not agent_output:
            time.sleep(3)
            agent_output = self._check()

        if is_k8s:
            agent_output = list(map(lambda x: x.decode('utf-8'), agent_output))

            self.lab_hash = list(filter(lambda x: 'Lab Hash' in x, agent_output))[0].replace('Lab Hash: ', '').strip()
            node = list(filter(lambda x: 'Assigned Node' in x, agent_output))[0].replace('Assigned Node: ', '').strip()

            self._check_and_start_service()

            self.host_url = 'http://%s:30000' % node
        else:
            self.host_url = 'http://localhost:3000'

        logging.info("Controller agent URL is %s" % self.host_url)

    def _check(self):
        command = "%s linfo -d %s -n controller_agent" % (KATHARA_PATH, self.lab_path)
        agent_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        agent_output = agent_process.stdout.readlines()[1:-1]
        agent_process.terminate()

        return agent_output

    def _start(self):
        command = "%s lstart --noterminals -d \"%s\"" % (KATHARA_PATH, self.lab_path)
        agent_process = Popen(shlex.split(command), stdout=DEVNULL, stderr=DEVNULL)
        agent_process.wait()
        agent_process.terminate()

    def _check_and_start_service(self):
        command = "kubectl -n %s get service controller-agent" % self.lab_hash
        process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        output = process.stdout.readlines()
        process.terminate()

        # Check if service is started (one line is header, other line is service)
        if len(output) < 2:
            os.system('cat %s/agent-controller.yaml | sed "s/__NAMESPACE__/%s/g" | kubectl create -f -' %
                      (self.lab_path, self.lab_hash)
                      )

    def get_internal_url(self, is_k8s):
        if not self._internal_url:
            self._internal_url = "http://%s:3000" % self._get_relative_url(is_k8s)

        return self._internal_url

    def _get_relative_url(self, is_k8s):
        if not is_k8s:
            process = Client.exec_async(self.lab_path, 'controller_agent', 'ip -j address show dev eth0', stdout=PIPE)
            output = process.stdout.readline().decode('utf-8')
            process.terminate()

            interface_info = list(filter(lambda x: x, json.loads(output)))[0]
            ipv4_address = [z for z in interface_info["addr_info"] if z['family'] == "inet"][0]
            return ipv4_address['local']
        else:
            return 'controller-agent'
